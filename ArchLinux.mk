uname_S := $(shell sh -c 'uname -s 2>/dev/null || echo not')

ifneq ($(uname_S),Linux)
	@echo "This is a '$(uname_S)' OS"
	@exit 2
endif

.PHONY: deps
deps:
	sudo pacman -S xorg-xinit xorg-xmodmap xorg-xrdb xorg-xfontsel \
	xorg-xsetroot xorg-xprop bspwm sxhkd xclip mplayer alsa-utils \
	alsa-tools feh dunst the_silver_searcher ranger ufw-extras fuse \
	gnupg openssh mosh firefox curl git wget mercurial subversion \
	highlight atool mediainfo libcaca imagemagick \
	ttf-ubuntu-font-family ttf-fira-sans ttf-fira-mono tree \
	xorg-server xorg-server-utils xorg-apps ttf-liberation \
	deluge mutt bash-completion vim sakura feh dmenu emacs \
	xf86-input-synaptics screen pinentry ncmpc libnotify tmux \
	newsbeuter numix-themes w3m tcl texlive-latexextra rsync \
	autoconf texinfo gv
