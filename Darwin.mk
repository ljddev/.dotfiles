## Copyright 2016 Lenilson Jose Dias. All rights reserved.
## Use of this source code is governed by a BSD-style
## license that can be found in the LICENSE file.


PRJ_DIR=${PWD}/home
HOME_DOT_FILES = .screenrc .tmux.conf .vimperatorrc .gitconfig .turses .mutt .gemrc

default: help


.PHONY: install
install: $(HOME_DOT_FILES)


.PHONY: $(HOME_DOT_FILES)
$(HOME_DOT_FILES):
	@ln -sf $(PRJ_DIR)/$@ ${HOME}/$<


.PHONY: help
help:
	@echo "Run 'make install' to create the symbolic links at '${HOME}' for: "
	@echo ""
	@echo $(HOME_DOT_FILES) | tr ' ' '\n' | sed -e 's/^/- /'
	@echo ""
	@echo "Run 'make deps' to install dependencies"


.PHONY: deps
deps: homebrew
	@brew install vim --with-lua
	@brew install fasd tree git mercurial go bash-completion imagemagick  the_silver_searcher homebrew/dupes/screen emacs wget gnupg weechat ranger w3m atool highlight mediainfo tmux
	@brew install mutt --build-from-source --with-gpgme --with-slang

.PHONY: python
python:
	@sudo easy_install pip
	@sudo pip install --upgrade pip setuptools
	@sudo pip install turses
	@sudo pip install goobook


.PHONY: homebrew
homebrew:
	@/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
