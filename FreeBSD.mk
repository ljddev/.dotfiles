PRJ_DIR=${PWD}/home
uname_S := $(shell sh -c 'uname -s 2>/dev/null || echo not')


.if ! $(OS) == FreeBSD
	@echo "This is not a FreeBSD."
	@exit 2
.endif


.PHONY: deps
deps: freebsd
	sudo pkg install xorg dmenu screen git \
	tmux xinit xclip dunst xrdb xsetroot xfontsel dbus curl wget \
	emacs24 gnupg mercurial subversion py27-ranger \
	terminus-font the_silver_searcher urwfonts dejavu gnupg \
	pinentry-gtk2 setxkbmap sudo liberation-fonts-ttf fira \
	deluge w3m file ko-nanumcoding-ttf turses ncdu alsa-utils \
	mutt vim sakura feh xpdf highlight atool tree \
	poppler-utils mediainfo sbcl zsh redshift libxul \
	password-store pinentry-tty ledger texlive-full \
	latex-mk chromium mplayer rsync firefox base64 \
	openjdk8 ssmtp py27-goobook libav

.PHONY: freebsd
freebsd:
	@cp $(PRJ_DIR)/.login_conf ${HOME}
	@ln -sf $(PRJ_DIR)/.shrc ${HOME}/.shrc
