## Copyright 2016 Lenilson Jose Dias. All rights reserved.
## Use of this source code is governed by a BSD-style
## license that can be found in the LICENSE file.


PRJ_DIR=${PWD}/home
HOME_DOT_FILES = .Xresources .Xmodmap .xinitrc .screenrc .tmux.conf .vimperatorrc .gitconfig .turses .mutt .gemrc .gtkrc-2.0 .stumpwmrc .start-stumpwm
DOT_CONFIG_FILES = bspwm sxhkd dunst sakura gtk-3.0


default: help


.PHONY: install
install: $(HOME_DOT_FILES) $(DOT_CONFIG_FILES)


.PHONY: $(HOME_DOT_FILES)
$(HOME_DOT_FILES):
	@ln -sf $(PRJ_DIR)/$@ ${HOME}/$<


.PHONY: $(DOT_CONFIG_FILES)
$(DOT_CONFIG_FILES):
	@mkdir -p ${HOME}/.config
	@ln -sf $(PRJ_DIR)/.config/$@ ${HOME}/.config/$<


.PHONY: help
help:
	@echo "Run 'make install' to create the symbolic links at '${HOME}' for: "
	@echo ""
	@echo $(HOME_DOT_FILES) | tr ' ' '\n' | sed -e 's/^/- /'
	@echo ""
	@echo "And symbolic links at '${HOME}/.config' for:"
	@echo ""
	@echo $(DOT_CONFIG_FILES) | tr ' ' '\n' | sed -e 's/^/- /'
	@echo ""
	@echo "To install specific OS dependencies have an OS environment variable"
	@echo "according *.mk files here."


.PHONY: deps
deps:
	@$(MAKE) -f $(OS).mk $@
