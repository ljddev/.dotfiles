# .dotfiles #

A set of dot-files for:

```
.
├── .Xmodmap
├── .Xresources
├── .config
│   ├── bspwm
│   │   └── bspwmrc
│   ├── dunst
│   │   └── dunstrc
│   └── sxhkd
│       └── sxhkdrc
├── .gitconfig
├── .login_conf
├── .screenrc
├── .shrc
├── .tmux.conf
├── .vimperatorrc
├── .xinitrc
```

## Installation

### FreeBSD 10.2

*IMPORTANT*: This will overwrite your .shrc and .login_conf and install some packages with pkg.

```
git clone https://github.com/0x7cc/.dotfiles
cd ~/.dotfiles/
make install       # installs dotfiles at home.
OS=FreeBSD make deps          # installs all dependencies at FreeBSD.
```

### Archlinux

```
git clone https://github.com/0x7cc/.dotfiles
cd ~/.dotfiles/
make install
OS=ArchLinux make deps       #install all dependencies at ArchLinux.
```

## License

Copyright 2015 Lenilson Jose Dias. All rights reserved.

Use of this source code is governed by a BSD-style license that can be found in the LICENSE file.
