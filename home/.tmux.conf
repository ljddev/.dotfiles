# -*-mode: conf; eval: (progn  (setq tab-width 8)(setq indent-tabs-mode nil));-*-
# vim: set et ts=8 nowrap: THPXRF E

################################################################
# File:             $HOME/.tmux.conf of Lenilson J. Dias       #
# Purpose:          Setup file for program "tmux"              #
# written by:       Lenilson Jose Dias lenilson(at)gmail.com   #
################################################################

set-option -g           default-shell $SHELL
set-window-option -g    utf8 on
set-option -ga          terminal-overrides ',*:enacs@:smacs@:rmacs@:acsc@'

# instructs tmux to expect UTF-8 sequences
setw -g          utf8 on
set  -g          status-utf8 on

# Use something easier to type as the prefix.
unbind C-b
set -g          prefix C-o

# Relax!
set -sg         escape-time 1
set -sg         repeat-time 600

# Shut up.
set -g          quiet on

# Mouse
set -g          mouse on

# Reload the config.
bind r source-file ~/.tmux.conf \; display "Reloaded ~/.tmux.conf"

# Saner splitting.
bind v		split-window -h
bind s		split-window -v
bind S		choose-session

# Pane movement
bind h		select-pane -L
bind j		select-pane -D
bind k		select-pane -U
bind l		select-pane -R

# DVTM style pane selection
bind 1		select-pane -t 1
bind 2		select-pane -t 2
bind 3		select-pane -t 3
bind 4		select-pane -t 4
bind 5		select-pane -t 5
bind 6		select-pane -t 6
bind 7		select-pane -t 7
bind 8		select-pane -t 8
bind 9		select-pane -t 9

# Layouts
set -g          main-pane-width 260
bind M		select-layout main-vertical
bind E		select-layout even-horizontal

# Pane resizing
bind -r C-h             resize-pane -L 5
bind -r C-j             resize-pane -D 5
bind -r C-k             resize-pane -U 5
bind -r C-l             resize-pane -R 5


# Window movement
# Only really makes sense if you have your parens bound to shifts like me.
bind -r i           select-window -t :-
bind -r o           select-window -t :+

# 256 colors please
set -g          default-terminal "screen-256color"

# COLORS.
# For reference, the xterm color cube points are: 00, 5F, 87, AF, D7, FF
# Status bar has a dim gray background
set-option -g               status-bg colour0
set-option -g               status-fg colour244
set-window-option -g        window-status-fg default
set-window-option -g        window-status-current-fg colour255
set-window-option -g        window-status-format '#I:#W '
set-window-option -g        window-status-current-format '#I:#W '

setw -g                     monitor-activity on
set -g                      visual-activity on

# Autorename
setw -g         automatic-rename on

# Better name management
bind c        new-window
bind ,        command-prompt "rename-window '%%'"

# act like vim
 setw -g        mode-keys vi
 bind h         select-pane -L
 bind j         select-pane -D
 bind k         select-pane -U
 bind l         select-pane -R
 unbind [
 bind-key \ copy-mode
 unbind p
 bind p paste-buffer
 bind -t vi-copy v begin-selection
 bind -t vi-copy y copy-selection
